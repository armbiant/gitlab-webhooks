import os
import subprocess

UPDATE_COMMAND = ['update_obis']

class InvalidCommandException(Exception):
    pass

def get_env(name: str): # pragma: no cover
    return os.environ[name]

def process_out(command: str): # pragma: no cover
    if command != UPDATE_COMMAND:
        raise InvalidCommandException
    subprocess.run(command)
