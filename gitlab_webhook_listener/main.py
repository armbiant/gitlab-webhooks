import logging
import json

from gitlab_webhook_listener.utils import get_env, process_out, InvalidCommandException, UPDATE_COMMAND
from http.server import BaseHTTPRequestHandler, HTTPServer

LOG_FILE = '/var/log/server.log'
LOG_FORMAT = '%(asctime)s, %(levelname)s %(message)s'
SERVER_ADDR = '0.0.0.0'
SERVER_PORT = 8000

logging.basicConfig(filename=LOG_FILE, filemode='a', format=LOG_FORMAT, level=logging.INFO)

class RequestHandler(BaseHTTPRequestHandler):
    def __send_error_response(self, code: int, message: str):
        logging.error(message)
        self.send_response(code, message)
        self.end_headers()

    def do_POST(self):
        logging.info('Gitlab Webhook received')

        self._gitlab_token = get_env('GITLAB_TOKEN')
        self._environment = get_env('ENVIRONMENT')

        header_length = int(self.headers['Content-Length'])
        gitlab_token_header = self.headers['X-Gitlab-Token']

        json_payload = self.rfile.read(header_length)
        json_params = {}

        if len(json_payload) > 0:
            json_params = json.loads(json_payload.decode('utf-8'))

        try:
            object_kind = json_params['object_kind']
        except KeyError:
            self.__send_error_response(500, 'No Webhook type provided by JSON payload')
            return

        if object_kind != 'deployment':
            self.__send_error_response(401, f'{object_kind} is not a supported Webhook type')
            return

        if gitlab_token_header != self._gitlab_token:
            self.__send_error_response(401, 'Gitlab token not authorized!')
            return

        deployment_id = json_params['deployment_id']
        project_name = json_params['project']['name']
        environment = json_params['environment']
        deployment_status = json_params['status']
        deployment_status_msg = f'deployment {deployment_id} for {project_name} in {environment} has status {deployment_status}'

        if environment != self._environment:
            self.__send_error_response(401, f'Running in {self._environment}, but received webhook for {environment}, ignoring webhook')
            return

        if deployment_status == 'failed':
            self.__send_error_response(500, deployment_status_msg)
            return
        elif deployment_status == 'success':
            logging.info(deployment_status_msg)
            try:
                logging.info(f'running command {UPDATE_COMMAND}')
                process_out(UPDATE_COMMAND)
            except OSError:
                self.__send_error_response(500, 'Command did not run successfully')
            except InvalidCommandException:
                self.__send_error_response(500, 'Invalid command passed to process_out')
            else:
                self.send_response(200, deployment_status_msg)
                self.end_headers()
            finally:
                return
        else:
            logging.info(f'{deployment_status_msg}, ignoring webhook')

def run(): # pragma: no cover
    httpd = HTTPServer((SERVER_ADDR, SERVER_PORT), RequestHandler)
    httpd.serve_forever()

if __name__ == "__main__":
    run()
