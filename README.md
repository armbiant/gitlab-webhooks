# Gitlab Webhook Listener
Listens for Gitlab webhooks and runs a script to update OBIS with the version built from the pipeline that triggered the webhook.

This is meant to be run as a sidecar service in a Docker container (example here: https://gitlab.com/oklahoma-biological-survey/obs-docker-images/-/tree/main/obis-gitlab-webhook-listener).

Currently, only supports updating OBIS (see https://gitlab.com/oklahoma-biological-survey/python/gitlab-webhook-listener/-/blob/main/gitlab_webhook_listener/utils.py#L4), but this is easily changed.
