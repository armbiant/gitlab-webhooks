# CHANGELOG

## Version 2.1.2 - December 13, 2022
    - Updating README

## Version 2.1.1 - November 7, 2022
    - Do need to return from `do_POST` when any response is sent

## Version 2.1.0 - November 7, 2022
    - Ensuring that deployment only occurs when environment matches

## Version 2.0.0 - November 7, 2022
    - Fixing bugs with webhook listener library
    - Restructuring code to make things more readable

## Version 1.0.3 - September 30, 2022
    - Added GNU GPLv3 license

## Version 1.0.2 - September 10, 2022
    - *Actually* fixing bug where command is `None`

## Version 1.0.1 - September 10, 2022
    - Fixing bug where command is `None`

## Version 1.0.0 - September 10, 2022
    - Fixing some logic errors
    - Added more logging messages

## Version 0.1.2 - September 10, 2022
    - Fixing bug where application doesn't actually run

## Version 0.1.1 - September 10, 2022
    - Fixing some logging

## Version 0.1.0 - August 31, 2022
    - Initial definition
